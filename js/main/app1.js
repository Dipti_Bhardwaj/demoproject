/**
 * Created by Dipti Bhardwaj on 7/24/2016.
 */

var dipti = angular.module('demoApp', ['ui.router']);
dipti.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('/', {
            url: '/',
            templateUrl: 'views/home.html',
            controller: 'my_controller'
        })
        .state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            controller: 'my_controller'
        })

        .state('detail', {
            url: '/detail/:id',
            templateUrl: 'views/detail.html',
            controller: 'detail'

        })

        .state('contact', {
            url: '/contact',
            templateUrl: 'views/contact.html',
            controller: 'contact'
        })
})

dipti.service('myService', ['$http', '$log', '$q', function ($http, $log, $q) {
    var defer = $q.defer();
    function successCallback(response) {
        $log.log("Here after success>>>>>>", response.data);
        defer.resolve(response.data);
        return response.data;
    }
    function errorCallback(response) {
        var error = response.statusText;
        defer.reject(response);
        $log.log("Here after error>>>>>>", error);
    }
    var getData2 = function () {
        $http({
            method: 'GET',
            url: 'http://jsonplaceholder.typicode.com/posts'
        }).then(successCallback, errorCallback);
        return defer.promise;
    }
    return {
        getData2: getData2
    };
}])


/*dipti.factory('myFactory',['$scope','$http','$q','$stateParams',function ($scope,$http,$q,$stateParams) {
 $scope.hereId = $stateParams.id;

 var defer = $q.defer();
 function success(response) {
 console.log("Here after success11111111111>>>>>>", response.data);
 defer.resolve(response.data);
 return response.data;
 }

 function error(response) {
 var error = response.statusText;
 defer.reject(response);
 console.log("Here after error222222222>>>>>>", error);
 }

 var getsinglerecord = function () {
 console.log('hgjhghgjgjhgk===========================',$scope.hereId)
 $http({
 method: 'GET',
 url: 'http://jsonplaceholder.typicode.com/posts/'+$scope.hereId
 }).then(success, error);

 return defer.promise;
 }

 return{
 getsinglerecord : getsinglerecord
 };
 }])*/

dipti.controller('my_controller', ['$scope', 'myService', '$log', function ($scope, myService, $log) {
    $scope.loading = true;
    myService.getData2().then(function (result) {
        $scope.loading = false;
        $scope.httpdata = result;
    }, function (errordata) {
        $scope.error = errordata;
    });
    $scope.settings = {
        loadMore: 'onScroll',
        bottomelementid: 'footer'
    };
    $scope.loadMoreUser = function (loadmore) {
        console.log(">>>>>>>>>load more>>>>>>>>>>>>")
        if($scope.stoploading){
            return;
        }
        if ($scope.loadingMore1) {
            $scope.loadingMore = false;
            return;
        }
        if (loadmore) {
            $scope.loadingMore1 = true;
        }
        myService.getData2().then(function (result) {
            $scope.loading = false;
            $scope.httpdata = result;
        }, function (errordata) {
            $scope.error = errordata;
        });


    }
    if ($scope.settings.loadMore == 'onScroll') {
        $(document).unbind('scroll');
        $(document).scroll(function () {
            var height = 0;
            if ($scope.settings.bottomelementid) {
                height = $('#' + $scope.settings.bottomelementid).height();
            }
            var windowScollTop = $(window).scrollTop();
            var winHeight = $(window).height();
            var docHeight = $(document).height();
            if ((windowScollTop >= ( (docHeight - winHeight) - height)) && (winHeight > 0) && (docHeight - winHeight) > height) {
                console.log(">>>>>>>>>>>>..sghfsgfhgashfbsahfshb>>>>>>.")
                if ($scope.settings.loadMore == 'onScroll') {
                    $scope.loadMoreUser(true);
                    $(window).scrollTop(windowScollTop);
                }
            }
        });
    }

}]);
dipti.controller('contact', ['$scope', function ($scope) {
    console.log('inside contact controller>>>>>>++++++++==============');
}]);
dipti.controller('detail', ['$scope', 'myService', '$stateParams', '$q', '$http', function ($scope, myService, $stateParams, $q, $http) {
    $scope.loading = true;
    $scope.hereId = $stateParams.id;
    var defer = $q.defer();

    function success(response) {
        defer.resolve(response.data);
        return response.data;
    }

    function error(response) {
        var error = response.statusText;
        defer.reject(response);
    }

    var gethttpData = function () {
        $http({
            method: 'GET',
            url: 'http://jsonplaceholder.typicode.com/posts/' + $scope.hereId
        }).then(success, error);
        return defer.promise;
    }
    gethttpData().then(function (result) {
        $scope.loading = false;
        $scope.item = result;
    }, function (errordata) {
        $scope.error = errordata;
    });
}])




